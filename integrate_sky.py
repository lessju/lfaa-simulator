from multiprocessing import pool
from process_visibilities import *
import numpy as np
import time
import h5py
import sys
import os

# Define some parameters
n_elevation = 257
n_azimuth = 1024
nof_elements = 256
nof_pols = 2
nof_baselines = nof_elements * (nof_elements + 1) / 2

# Filepath containing element patterns, sky and generated visibilities
# Shape is (n_elements, n_elevation, n_azimuth) for both theta and phi
element_filepath = 'element_patterns/AAVS1_XDipole_pattern_complex_voltages_160MHz_OSCAR.mat'

# Frequency we are operating in
c = 299792458.0
f = 160.0e6

# The sky
sky = None

# Theta and phi for each element
elements_theta = None
elements_phi = None

# Pre-compute sin(theta) for each elevation
sin_theta = np.sin(np.radians(np.arange(0, 90, 90.0 / n_elevation)))

# Output visibility placeholder
visibilities = np.zeros((nof_elements, nof_elements), dtype=np.complex128)

# Generate indices for require baselines
indices = [(ii, jj) for ii in range(nof_elements) for jj in range(ii, nof_elements)]

# Number of threads to use (1 = serial execution)
n_threads = 4

# Global counter for keeping track of progress
global_counter = 0


def integrate_pairwise(index):
    # Each thread is responsible for n_threads combinations (for speed)
    global global_counter
    global visibilities

    for k in range(n_threads):
        # Get element indices
        ant_i, ant_j = indices[index * n_threads + k]

        # Compute dot product for theta
        dot_theta = (elements_theta[:, :, ant_i] * elements_theta[:, :, ant_j].conj()) * sin_theta
        dot_phi = (elements_phi[:, :, ant_i] * elements_phi[:, :, ant_j].conj()) * sin_theta

        # Compute visibility
        visibilities[ant_i, ant_j] = np.sum(sky * (dot_theta + dot_phi))

        global_counter += 1

    sys.stdout.write("Finished processing {0} {1:.2f}%     \n".format(global_counter,
                                                                      (global_counter / float(nof_baselines)) * 100))
    sys.stdout.flush()


def integrate_element_sky():
    """ Perform integration over entire sky and all patterns.
        n_threads defines whether this is performed in parallel"""

    # Integrate over all baselines
    if n_threads != 1:
        thread_pool = pool.ThreadPool(n_threads)
        thread_pool.map(integrate_pairwise, range(0, nof_baselines / n_threads))
    else:
        for index in range(nof_baselines):
            integrate_pairwise(index)


def generate_average_element_pattern(use_flattened_pattern=False):
    """ Generate the average element pattern and set each element to the average """
    global coordinates
    global kx
    global ky

    k0 = 2 * np.pi / (c / f)
    n_thetas = n_elevation
    n_phis = n_azimuth

    # Load antenna locations
    coordinates = antenna_coordinates()

    # Compute kx and ky
    theta_range = np.radians(np.linspace(0, 90, n_thetas))
    phi_range = np.radians(np.linspace(0, 360, n_phis))
    kx = np.zeros((n_phis, n_thetas))
    ky = np.zeros((n_phis, n_thetas))
    for i in range(n_phis):
        for j in range(n_thetas):
            kx[i, j] = k0 * np.cos(phi_range[i]) * np.sin(theta_range[j])
            ky[i, j] = k0 * np.sin(phi_range[i]) * np.sin(theta_range[j])

    # If using flattened element pattern then we just assign the patterns to the location shift matrix
    if use_flattened_pattern:
        for i in range(nof_elements):
            # Compute total phase offset
            phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
            location_shift = np.exp(1j * phase_offset)

            # Phase shift the element pattern
            elements_theta[:, :, i] = location_shift
            elements_phi[:, :, i] = location_shift

        return  # All done

    # Loop over all the elements patters to compute average element pattern
    for i in range(nof_elements):
        # Compute total phase offset
        phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
        center_shift = np.exp(-1j * phase_offset)

        # Phase shift the element pattern
        elements_theta[:, :, i] = elements_theta[:, :, i] * center_shift
        elements_phi[:, :, i] = elements_phi[:, :, i] * center_shift

    # Compute average element pattern
    average_pattern_theta = np.mean(elements_theta, axis=2)
    average_pattern_phi = np.mean(elements_phi, axis=2)

    # Loop over all the elements patters to place phased average element pattern
    for i in range(nof_elements):
        # Compute total phase offset
        phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
        location_shift = np.exp(1j * phase_offset)

        # Phase shift the element pattern
        elements_theta[:, :, i] = average_pattern_theta * location_shift
        elements_phi[:, :, i] = average_pattern_phi * location_shift


def generate_visibilities(filepath, use_average_pattern=False):
    global visibilities
    global elements_theta
    global elements_phi
    global sky

    # Check if visibilities have already been processed, if so load the visibility file
    if os.path.exists(filepath):
        visibilities = np.load(filepath)
    else:
        # Load the pre-processed sky map and 'rotate' by 90 degrees
        sky = np.load(sky_filepath)
        sky = np.roll(sky, n_azimuth / 4, axis=0)

        # Load mat file containing element patterns
        with h5py.File(element_filepath, 'r') as element_file:
            # Load theta and phi and convert to numpy complex
            elements_theta = element_file['dipole_pattern_theta'][:].T
            elements_theta = elements_theta['real'] + elements_theta['imag'] * 1j
            elements_phi = element_file['dipole_pattern_phi'][:].T
            elements_phi = elements_phi['real'] + elements_phi['imag'] * 1j

        # Select required range from element patterns
        elements_theta = elements_theta[:n_azimuth, :n_elevation, :]
        elements_phi = elements_phi[:n_azimuth, :n_elevation, :]

        # If average pattern is required, compute element theta and phi averages
        if use_average_pattern:
            generate_average_element_pattern()

        # Perform integration
        t0 = time.time()
        integrate_element_sky()
        t1 = time.time()
        print "Generated visibilites in {0:.2f}s".format(t1 - t0)

        # Save visibilities
        np.save(filepath, visibilities)


if __name__ == "__main__":

    # Filepath to save (or read) visibilities
    visibilities_filepath = "simulated_visibilities/visibilities_embedded_pattern_galaxy_sky.npy"

    # Sky model to use
    sky_filepath = 'sky_models/sky_model_galaxy.npy'

    generate_visibilities(visibilities_filepath, use_average_pattern=False)
    plot_visbilities(visibilities)
