from math import sin, cos, radians

import numpy as np
from matplotlib import pyplot as plt

# AAVS Station center
aavs_station_latitude = -26.70408
aavs_station_longitude = 116.670231

# Some global params
antennas_per_tile = 16
nof_antennas = 256
nof_baselines = nof_antennas * (nof_antennas + 1) / 2

# Path to file containing antenna mapping
antenna_filepath = "antenna_locations/antenna_locations.csv"


def antenna_coordinates():
    """ Load antenna locations """
    antenna_positions = np.zeros((nof_antennas, 3))

    with open(antenna_filepath, 'r') as f:
        data = f.readlines()
        for i, location in enumerate(data):
            loc = location.split(',')
            antenna_positions[i, :2] = np.array([float(loc[1]), float(loc[2])])

    return antenna_positions


def calculate_uvw(dec, ha=0):
    """ Compute the UV plane given a pointing
    :param dec: Required Declination
    :param ha: Required hour angle
    :return: The full U-V plane (entire grid populated) """

    # Form rotation matrix (needs checking)
    dec = radians(dec)
    ha = radians(ha)
    rot_matrix = np.matrix([[sin(ha), cos(ha), 0],
                            [-sin(dec) * cos(ha), sin(dec) * sin(ha), cos(dec)],
                            [cos(dec) * cos(ha), -cos(dec) * sin(ha), sin(dec)]])

    # Get antenna locations
    antenna_mapping = antenna_coordinates()

    # Generate UV plane
    uvw_plane = np.zeros((nof_antennas * nof_antennas, 3))
    for i in range(nof_antennas):
        for j in range(nof_antennas):
            uvw_plane[i * nof_antennas + j, :] = (rot_matrix * np.matrix(antenna_mapping[i] - antenna_mapping[j]).T).T

    return uvw_plane


def calculate_baseline_length():
    """ Compute the length of each baseline
    :return: Array containing length of each baseline """

    # Get antenna locations
    antenna_mapping = antenna_coordinates()

    # Calculate length of each baseline
    counter = 0
    baseline_length = np.zeros(nof_baselines)
    for i in range(nof_antennas):
        for j in range(i, nof_antennas):
            val = np.matrix(antenna_mapping[i] - antenna_mapping[j]).T
            baseline_length[counter] = np.sqrt(val[0] * val[0] + val[1] * val[1])
            counter += 1

    return baseline_length


def plot_visbilities(data):

    # --------- Plot amplitude vs UV distance

    baselines = calculate_baseline_length()

    # Plot amplitude vs UV distance
    f = plt.figure(figsize=(14, 10))
    f.tight_layout()
    plt.scatter(baselines, np.abs(data[np.triu_indices(nof_antennas)]), s=0.2)
    plt.title("Ampltiude vs baseline distance (no auto)")
    plt.xlabel("Baseline distance (m)")
    plt.ylabel("Amplitude")

    # --------- Plot correlation matrix
    data = np.abs(data)
    f = plt.figure(figsize=(14, 10))
    data[np.where(data < .01)] = .01
    plt.imshow(10 * np.log10(np.abs(data)), aspect='auto')
    plt.xlabel("Antenna")
    plt.ylabel("Antenna")
    plt.colorbar()
    plt.title("Correlation Matrix (XX, log)")

    # --------- Show plots
    plt.show()


if __name__ == "__main__":
    print antenna_coordinates()