from matplotlib import pyplot as plt
import shutil

import numpy as np
import aipy
import os


class SimulationVisualiser:

    def __init__(self, visibilities):
        """ Class constructor"""

        # Define constants
        self._aavs_station_latitude = -26.7040
        self._aavs_station_longitude = 116.670
        self._nof_antennas = 256
        self._nof_baselines = self._nof_antennas * (self._nof_antennas + 1) / 2

        # Keep local copy of visibilities
        self._visibilities = visibilities

        # Antenna locations filename
        self._antenna_filepath = "antenna_locations/antenna_locations.csv"

        # Filename to store temporary uv file
        self._uv_filepath = "/tmp/aavs_simulator_uvfits.uv"

        # Generate antenna array and UV file
        self._antenna_array = self._generate_antenna_array()
        self._generate_uv_file()

        # Generate uvw
        self._uvw = self._generate_uvw()

    def _generate_antenna_array(self):
        # Read antenna locations file
        antenna_positions = np.zeros((self._nof_antennas, 3))

        with open(self._antenna_filepath, 'r') as f:
            data = f.readlines()[1:]
            for i, location in enumerate(data):
                loc = location.split(',')
                antenna_positions[i, :2] = np.array([float(loc[1]), float(loc[2])])

        # Set frequencies and beam
        freqs = np.array([0.160])
        beam = aipy.phs.Beam(freqs)

        antennas = []
        for i in range(self._nof_antennas):
            antennas.append(aipy.phs.Antenna(antenna_positions[i, 0] / 0.299792458,
                                             antenna_positions[i, 1] / 0.299792458,
                                             antenna_positions[i, 2] / 0.299792458,
                                             beam))

        return aipy.phs.AntennaArray(ants=antennas, location=(self._aavs_station_longitude,
                                                              self._aavs_station_latitude))

    def _generate_uv_file(self):
        """ Generate a UV file for simulated AAVS1 data"""

        # Create antenna array
        antenna_positions = np.zeros((self._nof_antennas, 3))

        with open(self._antenna_filepath, 'r') as f:
            data = f.readlines()[1:]
            for i, location in enumerate(data):
                loc = location.split(',')
                antenna_positions[i, :2] = np.array([float(loc[1]) / 0.299792458,
                                                     float(loc[2]) / 0.299792458])

        # If file exists, remove it
        if os.path.exists(self._uv_filepath):
            shutil.rmtree(self._uv_filepath)

        # Create myriad UV file
        uv = aipy.miriad.UV(self._uv_filepath, status='new')  # Start a new UV file
        uv['obstype'] = 'mixed-auto-cross'  # Miriad header item indicating data type
        uv['history'] = 'Created file'  # Record file creation for posterity

        # Create and initialize UV variables
        # a dict of variable data type is in a.miriad.data_types
        # This is not a complete list of variables, see MIRIAD programmer reference
        uv.add_var('epoch', 'r')  # Make a variable 'epoch', data type = real
        uv['epoch'] = 2000.  # Set epoch to 2000.
        uv.add_var('source', 'a')  # Source we are tracking, as a string
        uv['source'] = 'zenith'
        uv.add_var('latitud', 'd')  # Latitude of our array, as a double
        uv['latitud'] = self._aavs_station_latitude
        uv.add_var('longitu', 'd')  # Longitude of our array, as double
        uv['longitu'] = self._aavs_station_longitude
        uv.add_var('npol', 'i')  # Number of recorded polarizations, as int
        uv['npol'] = 1
        uv.add_var('nspect', 'i')  # Number of spectra recorded per antenna/baseline
        uv['nspect'] = 1
        uv.add_var('nants', 'i')  # Number of antennas in array
        uv['nants'] = 256
        uv.add_var('antpos', 'd')  # Positions (uvw) of antennas.  Expected to be 3*nants in length

        # Transposition is a MIRIAD convention.  You can follow it or not.
        uv['antpos'] = antenna_positions.transpose().flatten()

        uv.add_var('sfreq', 'd')  # Freq of first channel in spectra (GHz)
        uv['sfreq'] = .160
        uv.add_var('sdf', 'd')  # Delta freq between channels
        uv['sdf'] = .001
        uv.add_var('nchan', 'i')  # Number of channels in spectrum
        uv['nchan'] = 1
        uv.add_var('nschan', 'i')  # Number of channels in bandpass cal spectra
        uv['nschan'] = 1
        uv.add_var('inttime', 'r')  # Integration time (seconds)
        uv['inttime'] = 1.0

        # These variables will get updated every spectrum
        uv.add_var('time', 'd')
        uv.add_var('lst', 'd')
        uv.add_var('ra', 'd')
        uv.add_var('obsra', 'd')
        uv.add_var('baseline', 'r')
        uv.add_var('pol', 'i')

        # Now start generating data
        # Time steps in julian date
        times = [2454500]
        for cnt, t in enumerate(times):
            uv['lst'] = 0.  # Should be sidereal time from AntennaArray
            uv['ra'] = 0.  # RA of source you're pointing at
            for i, ai in enumerate(antenna_positions):
                for j, aj in enumerate(antenna_positions):
                    if j < i: continue
                    crd = ai - aj  # Find uvw coordinate of baseline
                    preamble = (crd, t, (i, j))  # Set preamble to (uvw, julian date, baseline)
                    uv['pol'] = aipy.miriad.str2pol['xx']  # Fix polarization as 'xx'
                    data = np.array([self._visibilities[i, j]])  # Generate some data
                    flags = np.zeros((uv['nchan'],), dtype=np.int32)  # Generate some flags (zero = valid)
                    uv.write(preamble, data, flags)  # Write this entry to the UV file
        del uv

    def _generate_uvw(self):
        """ Generate uvw """
        counter = 0
        uvw = np.zeros((self._nof_antennas ** 2, 3))
        for i in range(self._nof_antennas):
            for j in range(self._nof_antennas):
                uvw[counter, :] = np.squeeze(self._antenna_array.gen_uvw(i, j))
                counter += 1
        return uvw

    def plot_correlation_matrix(self):
        """ Plot correlation matrix"""
        plt.figure()
        data = np.abs(self._visibilities)
        data[np.where(data < .01)] = .01
        plt.imshow(10 * np.log10(np.abs(data)), aspect='auto')
        plt.title("Correlation matrix")
        plt.xlabel("Antenna")
        plt.ylabel("Antenna")
        plt.colorbar()
        plt.title("Correlation Matrix (XX, log)")
        plt.draw()

    def plot_uv_plane(self):
        """ Plot UV plane matrix"""

        plt.figure()
        plt.scatter(self._uvw[:, 0], self._uvw[:, 1], s=0.2)
        plt.title("UV plane")
        plt.draw()

    def plot_amplitude_vs_uv(self):
        """ Plot amplitude vs UV distance """
        plt.figure()
        plt.scatter(np.sqrt(self._uvw[:, 0] ** 2 + self._uvw[:, 1] ** 2), np.imag(self._visibilities).flatten(), s=0.2)
        plt.title("Ampltiude vs UV distance")
        plt.xlabel("U-V distance")
        plt.ylabel("Amplitude")

    def plot_sky_image(self):
        """ Generate image of the sky"""
        uv = aipy.miriad.UV(self._uv_filepath)

        data, uvw, wgts = [], [], []
        for (_, t, (i, j)), d in uv.all():
            crd = self._antenna_array.gen_uvw(i, j)
            uvw.append(np.squeeze(crd))
            data.append(d.compressed())
            wgts.append(np.array([1.] * len(data[-1])))

        data = np.concatenate(data)
        uvw = np.array(uvw)
        wgts = np.concatenate(wgts)

        plt.figure()
        im = aipy.img.Img(size=400, res=0.2)
        uvw, data, wgts = im.append_hermitian(uvw.T, data, wgts=wgts)
        im.put(uvw, data, wgts=wgts)
        plt.imshow(np.log10(im.image(center=(800, 800))))
        plt.draw()

    @staticmethod
    def wait_for_plots():
        plt.show()


if __name__ == "__main__":
    visualiser = SimulationVisualiser(np.load("simulated_visibilities/visibilities_flat_pattern_no_sky.npy"))
    # visualiser.plot_correlation_matrix()
    # visualiser.plot_uv_plane()
    visualiser.plot_amplitude_vs_uv()
    # visualiser.plot_sky_image()
    visualiser.wait_for_plots()
