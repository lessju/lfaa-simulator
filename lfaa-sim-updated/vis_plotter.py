import numpy as np
import matplotlib.pyplot as plt
import glob

text_file = np.genfromtxt('antenna_locations.csv', delimiter=',')
antennas = text_file[1:, 1:]
fig, ax = plt.subplots()
ax.scatter(antennas[:, 0], antennas[:, 1], c='black')
ax.scatter(antennas[110, 0], antennas[110, 1], c='red')
ax.scatter(antennas[128, 0], antennas[128, 1], c='red')
ax.scatter(antennas[3, 0], antennas[3, 1], c='green')
ax.scatter(antennas[252, 0], antennas[252, 1], c='green')
ax.scatter(antennas[116, 0], antennas[116, 1], c='blue')
ax.scatter(antennas[122, 0], antennas[122, 1], c='blue')
ax.scatter(antennas[81, 0], antennas[81, 1], c='orange')
ax.scatter(antennas[168, 0], antennas[168, 1], c='orange')

for i, txt in enumerate(range(256)):
    ax.annotate(txt, (antennas[i, 0], antennas[i, 1]))

files_path = 'C:/Users/Josef Borg/Documents/PythonProjects/AAVS_Calibration/sky_model/SM_Sun_60deg/'
vis_1 = []
vis_2 = []
vis_3 = []
vis_4 = []
vis_all = []

for filename in sorted(glob.glob(files_path + '*vis_*.npy')):
    vis_1.append((np.load(filename)[110, 128]).imag)
    vis_2.append((np.load(filename)[3, 252]).imag)
    vis_3.append((np.load(filename)[116, 122]).imag)
    vis_4.append((np.load(filename)[81, 168]).imag)
    vis_all.append(np.abs(np.load(filename)[:, :]))

vis_1 = np.array(vis_1)
vis_2 = np.array(vis_2)
vis_3 = np.array(vis_3)
vis_4 = np.array(vis_4)
# vis_all = np.array(vis_all)

# smooth_level = 8
# smooth = np.int(smooth_level/2)
# for i in range(smooth, len(vis_1)-smooth):
#     vis_1[i] = np.sum(vis_1[i-smooth:i+smooth])/(smooth*2)
#     vis_2[i] = np.sum(vis_2[i-smooth:i+smooth])/(smooth*2)
#     vis_3[i] = np.sum(vis_3[i-smooth:i+smooth])/(smooth*2)
#     vis_4[i] = np.sum(vis_4[i-smooth:i+smooth])/(smooth*2)

plt.figure()
plt.plot(vis_1, c='red')
plt.plot(vis_2, c='green')
plt.plot(vis_3, c='blue')
plt.plot(vis_4, c='orange')
# for i in range(vis_all.shape[1]):
#    for j in range(vis_all.shape[2]):
#        plt.plot((vis_all[:, i, j]))
# plt.legend(('110, 128', '3, 252', '116, 122', '81, 168'))
# plt.show()

vis_out = None
for filename in sorted(glob.glob(files_path + '*vis_*.npy')):
    print filename
    data_in = np.load(filename)
    vis_out = np.zeros(np.int(0.5*((data_in.shape[0] ** 2) - data_in.shape[0])))
    print 'vis_out: ' + str(vis_out.shape)
    counter = 0
    for i in range(np.load(filename).shape[0]):
        for j in range(np.load(filename).shape[1]):
            if i < j:
                vis_out[counter] = np.sqrt(((data_in[i, j].real**2) + (data_in[i, j].imag**2)))
                # vis_out[counter] = (data_in[i, j].imag)
                counter += 1

vis_out = np.array(vis_out)

calc_baselines = np.zeros(np.int(0.5*((antennas.shape[0] ** 2)-antennas.shape[0])))
print 'calc_baselines: ' + str(calc_baselines.shape)
counter = 0
for i in range(antennas.shape[0]):
    for j in range(antennas.shape[0]):
        if i < j:
            calc_baselines[counter] = np.sqrt(((antennas[i, 1] - antennas[j, 1])**2) +
                                              ((antennas[i, 0] - antennas[j, 0])**2))
            counter += 1

calc_baselines = np.array(calc_baselines)
# calc_baselines[np.isnan(calc_baselines)] = 0.

# Select baselines
# vis_to_plot = []
# bas_to_plot = []
# for i in range(antennas.shape[0]):
#     for j in range(i, antennas.shape[0]):
#         vis_to_plot.append(vis_out[0, i, j])
#         bas_to_plot.append(calc_baselines[i, j])

vis_to_plot = vis_out
bas_to_plot = calc_baselines

plt.figure()
plt.scatter(bas_to_plot, vis_to_plot, marker='.', s=0.3)
# plt.ylim(0, 60000)
# plt.xlim(0, 30)
plt.show()
