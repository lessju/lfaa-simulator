import numpy as np
from configparser import ConfigParser
from integrate_sky import IntegrateSky
from argparse import ArgumentParser


# Reading arguments
parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename",
                    help="configuration filename")
input_options = parser.parse_args()

filename = 'simulator_settings.ini'

# Reading config file
config = ConfigParser()
config.read(filename)

IntegrateSky(filename)
