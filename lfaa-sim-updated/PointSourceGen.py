import scipy.io as sio
import numpy as np
import datetime
from math import *
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
from scipy import spatial
import logging

diff_deg = .3515625

# Get RA Dec and closest corresponding sky power for defined El Az coordinates
el = np.arange(0., 90. + diff_deg, diff_deg)
az = np.arange(0., 360., diff_deg)

# Compute spherical map
data_labels_dec_rad = np.radians(el)
data_labels_ra_rad = np.radians(az)

# Determine x,y,z spherical projection of RA, Dec values
theta, phi = data_labels_dec_rad, data_labels_ra_rad
x = np.outer(np.cos(theta), np.cos(phi)).T
y = np.outer(np.cos(theta), np.sin(phi)).T
z = np.outer(np.ones(len(phi)), np.sin(theta))

sun_m = 1.3e4  # 1.3e4 is Sun brightness temp, at 32 arc seconds diameter, 160MHz (187cm) (10,000Jy)
alts = np.append(np.arange(60., 90., .5), np.flip(np.arange(60., 91., .5)))
azims = np.zeros(len(alts)) + 270.
azims[len(azims)/2:len(azims)] -= 180.

for i in range(len(alts)):
    d = np.zeros((x.shape[0], x.shape[1]))
    d[np.int(azims[i]*(1./diff_deg)), np.int(alts[i]*(1./diff_deg))-1] = sun_m

    output = np.array([x, y, z, d])

    fig = plt.figure()
    plt.imshow(d, cmap='magma')
    # ax = fig.add_subplot(111, projection='3d')
    # ax.set_aspect('auto')
    # ax.plot_surface(output[0], output[1], output[2], rstride=1, cstride=1,
    #                facecolors=cm.magma(np.log(output[3])),
    #                alpha=0.3, linewidth=1, shade=True)
    plt.savefig('sky_model/SM_Sun_60deg2/sky_model_' + str(i).zfill(3) + '_Sun_az' + str(azims[i]) +
               '_alt' + str(alts[i]) + '.png', dpi=300)
    np.save('sky_model/SM_Sun_60deg2/sky_model_' + str(i).zfill(3) + '_Sun_az' + str(azims[i]) +
            '_alt' + str(alts[i]) + '.npy', output)
    # plt.show()
    plt.close()




