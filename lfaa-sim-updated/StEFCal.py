import numpy as np
from matplotlib import pyplot as plt
from configparser import ConfigParser
import logging
import datetime
from process_visibilities import *
from sys import stdout


class StEFCal:

    def __init__(self, config_file):

        config = ConfigParser()
        config.read(config_file)

        log = logging.getLogger('')
        log.setLevel(logging.DEBUG)
        str_format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        ch = logging.StreamHandler(stdout)
        ch.setFormatter(str_format)
        log.addHandler(ch)
        self._logger = logging.getLogger(__name__)

        model = config.get('Input', 'model_visibilities').encode('ascii', 'ignore')
        observed = config.get('Input', 'observed_visibilities').encode('ascii', 'ignore')

        algorithm = config.get('Settings', 'algorithm').encode('ascii', 'ignore')
        tolerance = np.float(config.get('Settings', 'convergence_tolerance').encode('ascii', 'ignore'))
        max_iter = np.int(config.get('Settings', 'max_iterations').encode('ascii', 'ignore'))

        antenna_no = np.int(config.get('Telescope', 'antenna_number').encode('ascii', 'ignore'))
        locations = config.get('Telescope', 'antenna_locations').encode('ascii', 'ignore')

        errors_in = np.ones(antenna_no, dtype=np.complex)
        test = False
        if str(config.get('Testing', 'test_run').encode('ascii', 'ignore')) == 'True':
            test = True
            errors_in = config.get('Testing', 'errors').encode('ascii', 'ignore')
            self.plotting_scale = config.get('Testing', 'plot_scale').encode('ascii', 'ignore')
            self.bp_type = config.get('Testing', 'bp_type').encode('ascii', 'ignore')

        self.solutions = None
        self.calibration_score = None
        self.calibration_scores = None
        self.calibration_success = False
        self.calibration_info = list()

        self._run_alg(alg=algorithm, model_in=model, observed_in=observed, test=test, errors=errors_in,
                      max_iterations=max_iter, tol=tolerance, antenna_locations=locations)

    def _gain_solve(self, tolerance, model, measured, initial_solutions, max_iterations, baselines):
        # initialization
        nof_elements = len(initial_solutions)
        solutions = initial_solutions
        normalization_factor = np.zeros(nof_elements)
        normalised_measured = measured.copy()
        new_solutions = None

        # normalize the measured array covariance  matrix(weighting)
        for idx in range(nof_elements):
            normalization_factor[idx] = np.matmul(measured[:, idx].conj().T, measured[:, idx]).real
            if normalization_factor[idx] != 0:
                normalised_measured[:, idx] = measured[:, idx] / normalization_factor[idx]
            else:
                normalised_measured[:, idx] = 0.

        # initial calibration
        calibrated_model = np.zeros((nof_elements, nof_elements), dtype=np.complex)
        for idx in range(nof_elements):
            calibrated_model[:, idx] = solutions * model[:, idx]

        # start iterating
        calibration_estimate = np.zeros(nof_elements, dtype=np.complex)
        for iterate in range(1, max_iterations):
            # update gain estimate
            for idx in range(nof_elements):
                calibration_estimate[idx] = np.matmul(normalised_measured[:, idx].conj().T, calibrated_model[:, idx])

            for i in range(calibration_estimate.shape[0]):
                if calibration_estimate[i] != 0. + 0.j:
                    solutions[i] = 1 / np.conj(calibration_estimate[i])
                else:
                    calibration_estimate[i] = 0.

            if iterate % 2 > 0:
                # find new convergence curve
                new_solutions = solutions.copy()
            else:
                # update calibration results so far and check for convergence
                gold = solutions.copy()
                solutions = (solutions + new_solutions) / 2
                if iterate >= 2:
                    delta_solutions_norm = np.linalg.norm(solutions - gold)
                    solutions_norm = np.linalg.norm(solutions)
                    if delta_solutions_norm / solutions_norm <= tolerance:
                        self.calibration_info.append('convergence reached after {} iterations'.format(iterate))
                        self.calibration_info.append('relative change in norm(g) in the last iteration: {}'
                                                     .format(delta_solutions_norm / solutions_norm))
                        delta_cov_mod_before = np.abs(np.sqrt(np.mean((measured - model) ** 2)))
                        calibration_score_0 = delta_cov_mod_before / np.abs(np.sqrt(np.mean(model ** 2))) * 100
                        for xx in range(256):
                            for yy in range(xx, 256):
                                measured[xx, yy] *= (1 / (solutions[xx] * np.conj(solutions[yy])))
                        delta_cov_mod = np.abs(np.sqrt(np.mean((measured - model) ** 2)))
                        calibration_score_1 = delta_cov_mod / np.abs(np.sqrt(np.mean(model ** 2))) * 100
                        self.calibration_info.append('mean absolute percentage residual visibility delta before and '
                                                     'after calibration: ''{}%, {}%'.format(calibration_score_0,
                                                                                            calibration_score_1))
                        self.calibration_scores = [np.round((100 - calibration_score_0), 2),
                                                   np.round((100 - calibration_score_1), 2)]
                        # self._logger.info('Calibration improvement score: ' +
                        #                   '{:.2f}'.format(np.round((((calibration_score[0] - calibration_score[1]) /
                        #                                             calibration_score[0]) * 100.), 2)) + '%')
                        self.calibration_success = True
                        break

            # update calibration of array covariance matrix
            for idx in range(nof_elements):
                calibrated_model[:, idx] = solutions * model[:, idx]

            if iterate == max_iterations-1:
                self._logger.error('Calibration failed to converge to tolerance level {} after {} iterations'.format(
                    tolerance, max_iterations))

        self.solutions = solutions
        print self.solutions

    def _run_alg(self, model_in, observed_in, errors, antenna_locations,
                 test, tol=1e-4, max_iterations=8000, alg='StEFCal'):

        # Load model and noisy/real visibilities
        model = np.load(model_in)
        observed = np.load(observed_in)

        # Filter baselines with length < lambda
        c = 299792458.0
        f = 160.0e6
        lmbd = c / f

        baselines = np.ones((256, 256))
        baselines[np.triu_indices(256)] = calculate_baseline_length()
        baselines[np.where(baselines < lmbd * 2)] = 0
        model *= baselines
        observed *= baselines

        # Load errors given
        if type(errors) is str:
            errors_in = np.load(errors)

        if alg == 'StEFCal':

            # Define initial gain estimates as 1+0j
            gains_estimate = np.ones(256, dtype=np.complex)

            # Run StEFCal
            calibration_start = datetime.datetime.now()
            self._gain_solve(model=model, measured=observed, initial_solutions=gains_estimate,
                             tolerance=tol, max_iterations=max_iterations, baselines=baselines)
            conv_time = datetime.datetime.now() - calibration_start

            if self.calibration_success is True:
                self.calibration_score = self.calibration_scores[1]
                self._logger.info('Calibration finished successfully, {} in {}s'.format(
                                  self.calibration_info[0], '{:.2f}'.format(conv_time.seconds +
                                                                            conv_time.microseconds*1e-6)))
                self._logger.info('Pre-calibration mean power similarity score: ' +
                                  '{:.2f}'.format(self.calibration_scores[0]) + '%')
                self._logger.info('Post-calibration mean power similarity score: ' +
                                  '{:.2f}'.format(self.calibration_scores[1]) + '%')

            # Check antenna coefficients for coupling effects
            antenna_positions = np.zeros((256, 3))

            with open(antenna_locations, 'r') as f:
                data = f.readlines()[1:]
                for i, location in enumerate(data):
                    loc = location.split(',')
                    antenna_positions[i, :2] = np.array([float(loc[1]), float(loc[2])])

            # plot gain solutions

            # fig, ax = plt.subplots(2)
            # ax[0].set_aspect('equal')
            # ax[1].set_aspect('equal')
            #
            # x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.abs(self.solutions[:])
            # plot_gain_errors = ax[0].scatter(x, y, c=z, cmap='Reds')
            # ax[0].set_title('Gain Solutions', size=10)
            # plt.colorbar(plot_gain_errors, ax=ax[0], orientation='vertical')
            #
            # # plot phase solutions
            #
            # x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.angle(self.solutions[:], deg=True)
            # plot_phase_errors = ax[1].scatter(x, y, c=z, cmap='Reds')
            # ax[1].set_title('Phase Solutions', size=10)
            # plt.colorbar(plot_phase_errors, ax=ax[1], orientation='vertical')
            #
            # ax[0].tick_params(size=5)
            # ax[1].tick_params(size=5)
            #
            # plt.show()

            if test is True:

                fig, ax = plt.subplots(2, 2)
                ax[0, 0].set_aspect('equal')
                ax[0, 1].set_aspect('equal')
                ax[1, 0].set_aspect('equal')
                ax[1, 1].set_aspect('equal')

                # plot gain errors

                x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.abs(errors_in[:])
                plot_gain_errors = ax[0, 0].scatter(x, y, c=z, cmap='Greens', s=10)
                ax[0, 0].set_title('Gain Errors', size=9)
                ax[0, 0].tick_params(labelsize=8)

                cbar = plt.colorbar(plot_gain_errors, ax=ax[0, 0], orientation='vertical')
                cbar.ax.tick_params(labelsize=7)

                # plot phase errors

                x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.degrees(np.angle(errors_in[:]))
                plot_phase_errors = ax[0, 1].scatter(x, y, c=z, cmap='Greens', s=10)
                ax[0, 1].set_title('Phase Errors', size=9)
                ax[0, 1].tick_params(labelsize=8)

                cbar = plt.colorbar(plot_phase_errors, ax=ax[0, 1], orientation='vertical')
                cbar.ax.tick_params(labelsize=7)

                # plot gain residuals
                x, y, z_err = antenna_positions[:, 0], antenna_positions[:, 1], np.abs(errors_in[:])

                z_resid = np.abs(np.abs(self.solutions[:]) - z_err)
                # plt.figure()
                # plt.plot(z_err, linewidth=0.3, alpha=0.5, c='red')
                # plt.plot(np.abs(np.abs(self.solutions[:])), linewidth=0.3, alpha=0.5, c='blue')
                # plt.show()

                # flag_antennas = list(np.where(z_ratio >= .1)[0])
                # flag_antennas = np.array(flag_antennas, dtype=np.int)
                # z_resid[flag_antennas] = 0.

                if self.plotting_scale == 'log':
                    z_resid = np.log(z_resid)

                plot_gains = ax[1, 0].scatter(x, y, c=z_resid, cmap='Greens', s=10)
                # flag_antennas = np.array(flag_antennas, dtype=np.int)
                # ax[1, 0].scatter(x[flag_antennas], y[flag_antennas], c='red', s=8,  marker='x')
                ax[1, 0].tick_params(labelsize=8)

                ax[1, 0].set_title('Residual Gain Errors', size=9, y=-0.3)
                cbar = plt.colorbar(plot_gains, ax=ax[1, 0], orientation='vertical')
                cbar.ax.tick_params(labelsize=7)

                # plot phase residuals
                x, y, z_err = antenna_positions[:, 0], antenna_positions[:, 1], np.degrees(np.angle(errors_in[:]))

                z_resid = z_err - np.degrees(np.angle(self.solutions[:]))
                # plt.figure()
                # plt.plot(z_err, linewidth=0.3)
                # plt.plot(np.degrees(np.angle(self.solutions[:])), linewidth=0.3)
                # plt.show()

                # flag_antennas = list(np.where(np.abs(z_resid) >= 5.)[0])
                # flag_antennas = np.array(flag_antennas, dtype=np.int)
                # z_resid[flag_antennas] = 0.

                if self.plotting_scale == 'log':
                    z_resid = np.log(z_resid)

                plot_phases = ax[1, 1].scatter(x, y, c=z_resid, cmap='Greens', s=10)
                # ax[1, 1].scatter(x[flag_antennas], y[flag_antennas], c='red', s=8, marker='x')
                ax[1, 1].tick_params(labelsize=8)

                ax[1, 1].set_title('Residual Phase Errors', size=9, y=-0.3)
                cbar = plt.colorbar(plot_phases, ax=ax[1, 1], orientation='vertical')
                cbar.ax.tick_params(labelsize=7)

                ax[0, 0].tick_params(size=5)
                ax[0, 1].tick_params(size=5)
                ax[1, 0].tick_params(size=5)
                ax[1, 1].tick_params(size=5)

                plt.show()
                fig.savefig(str(errors)[:-4] + '_' + str(self.bp_type) + '_no_threshold.png', dpi=300)

            return self.solutions, self.calibration_info

        if alg == 'test':
            # test
            baseline_coeffs = np.ones(((observed_in.shape[0]**2 - observed_in.shape[0])/2), dtype=np.complex)
            baseline_coeffs_real = np.ones((observed_in.shape[0], observed_in.shape[1]), dtype=np.float)
            baseline_coeffs_imag = np.ones((observed_in.shape[0], observed_in.shape[1]), dtype=np.float)

            counter = 0
            for i in range(observed_in.shape[0]):
                for j in range(i+1, observed_in.shape[1]):
                    if np.abs(observed_in[i, j].real) > 0:
                        baseline_coeffs_real[i, j] = model_in[i, j].real / observed_in[i, j].real
                    if observed_in[i, j].real == 0.:
                        baseline_coeffs_real[i, j] = 1.
                    if np.abs(observed_in[i, j].imag) > 0:
                        baseline_coeffs_imag[i, j] = model_in[i, j].imag / observed_in[i, j].imag
                    if observed_in[i, j].imag == 0.:
                        baseline_coeffs_imag[i, j] = 0.
                    baseline_coeffs[counter] = np.complex(baseline_coeffs_real[i, j], baseline_coeffs_imag[i, j])
                    counter += 1

            a_mat = np.zeros(((observed_in.shape[0]**2 - observed_in.shape[0])/2, observed_in.shape[0]))
            b_vec = np.zeros((observed_in.shape[0]**2 - observed_in.shape[0])/2, dtype=np.complex)
            counter = 0
            for i in range(0, (observed_in.shape[0] - 1)):
                for j in range(i+1, observed_in.shape[0]):
                    a_mat[counter, i] = 1
                    a_mat[counter, j] = 1
                    with np.errstate(divide='ignore', invalid='ignore'):
                        b_vec[counter] = np.log(baseline_coeffs[counter])
                    counter += 1
            with np.errstate(divide='ignore', invalid='ignore'):
                log_coeffs = np.linalg.lstsq(a_mat, b_vec)[0]
            solutions = np.exp(log_coeffs)

            solutions /= solutions[0]

            # Check antenna coefficients for coupling effects
            antenna_positions = np.zeros((256, 3))

            with open('C:/Users/Josef Borg/Desktop/Resources+Results/antenna_locations.csv', 'r') as f:
                data = f.readlines()[1:]
                for i, location in enumerate(data):
                    loc = location.split(',')
                    antenna_positions[i, :2] = np.array([float(loc[1]), float(loc[2])])

            fig, ax = plt.subplots(2, 2)
            ax[0, 0].set_aspect('equal')
            ax[0, 1].set_aspect('equal')
            ax[1, 0].set_aspect('equal')
            ax[1, 1].set_aspect('equal')

            # plot gain errors

            x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.abs(errors[:])
            plot_gain_errors = ax[0, 0].scatter(x, y, c=z, cmap='Reds')
            ax[0, 0].set_title('Gain Errors', size=10)
            plt.colorbar(plot_gain_errors, ax=ax[0, 0], orientation='vertical')

            # plot phase errors

            x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], np.degrees(np.angle(errors[:]))
            plot_phase_errors = ax[0, 1].scatter(x, y, c=z, cmap='Reds')
            ax[0, 1].set_title('Phase Errors', size=10)
            plt.colorbar(plot_phase_errors, ax=ax[0, 1], orientation='vertical')

            # plot gain residuals
            x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], solutions[:].real

            z_new = np.abs(np.abs(solutions[:]) - np.abs(errors[:]))
            plot_gains = ax[1, 0].scatter(x, y, c=z_new, cmap='Reds')
            ax[1, 0].set_title('Residual Gain Errors', size=10, y=-0.3)
            plt.colorbar(plot_gains, ax=ax[1, 0], orientation='vertical')

            # plot phase residuals
            x, y, z = antenna_positions[:, 0], antenna_positions[:, 1], solutions[:].imag

            z_new = np.degrees(np.angle(solutions[:])) - np.degrees(np.angle(errors[:]))
            plot_phases = ax[1, 1].scatter(x, y, c=z_new, cmap='Reds')
            ax[1, 1].set_title('Residual Phase Errors', size=10, y=-0.3)
            plt.colorbar(plot_phases, ax=ax[1, 1], orientation='vertical')

            ax[0, 0].tick_params(size=5)
            ax[0, 1].tick_params(size=5)
            ax[1, 0].tick_params(size=5)
            ax[1, 1].tick_params(size=5)

            plt.show()
            return solutions


if __name__ == "__main__":
    StEFCal('config.ini')
