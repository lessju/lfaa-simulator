from multiprocessing import pool
from process_visibilities import *
import numpy as np
import time
import h5py
import sys
import os
from configparser import ConfigParser
import distutils
from datetime import datetime, timedelta
from SkyModelGen import SkyModelGenRun
import glob
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
import logging


# Define some parameters
n_elevation = 257
n_azimuth = 1024
nof_elements = 256
nof_pols = 2
nof_baselines = nof_elements * (nof_elements + 1) / 2
c = 299792458.0


class IntegrateSky:

    def __init__(self, configuration_file):

        self.config = ConfigParser()
        self.config.read(configuration_file)

        self.start_time = (np.str(self.config.get('Observation', 'start_time_utc')))
        self.end_time = (np.str(self.config.get('Observation', 'end_time_utc')))
        self.time_interval_hr = np.float(self.config.get('Observation', 'time_interval_s')) / 3600.
        self.sky_model_dir = np.str(self.config.get('Observation', 'sky_model_dir'))

        # Filepath containing element patterns, sky and generated visibilities
        # Shape is (n_elements, n_elevation, n_azimuth) for both theta and phi
        self.element_filepath = self.config.get('Input', 'element_filepath')

        # Frequency we are operating in
        self.f = np.float(self.config.get('Settings', 'frequency'))

        # The sky
        self.sky = None

        # Theta and phi for each element
        self.elements_theta = None
        self.elements_phi = None

        # Spherical beam placeholders
        self.beam_x = None
        self.beam_y = None
        self.beam_z = None

        # Pre-compute sin(theta) for each elevation
        self.sin_theta = np.sin(np.radians(np.arange(0, 90, 90.0 / n_elevation)))

        # Output visibility placeholder
        self.visibilities = np.zeros((nof_elements, nof_elements), dtype=np.complex128)

        # Generate indices for require baselines
        self.indices = [(ii, jj) for ii in range(nof_elements) for jj in range(ii, nof_elements)]

        # Number of threads to use (1 = serial execution)
        self.n_threads = np.int(self.config.get('Settings', 'num_threads'))

        # Global counter for keeping track of progress
        self.global_counter = 0

        # Create, or read, sky model
        self.sky_model_generation = distutils.util.strtobool(np.str(self.config.get('Settings', 'generate_sky_models')))
        self.num_time_steps = None
        self.current_time_step = list()
        self.sky_files = list()
        if self.sky_model_generation:
            self.create_sky_model()

        # Generate and save errors, if real visibilities simulation is chosen, otherwise 1+0j errors are defined
        self.errors = None
        self.generate_errors()

        self.visibilities_filepath = None
        self.sky_filepath = None
        self.start_simulation()

    def start_simulation(self):

        if self.sky_model_generation:
            for i in range(len(self.current_time_step)):

                # Filepath to save (or read) visibilities
                sky_model_time = '{}_{}_{}_{}_{}_{}'.format(str(self.current_time_step[i].year).zfill(2),
                                                            str(self.current_time_step[i].month).zfill(2),
                                                            str(self.current_time_step[i].day).zfill(2),
                                                            str(self.current_time_step[i].hour).zfill(2),
                                                            str(self.current_time_step[i].minute).zfill(2),
                                                            str(self.current_time_step[i].second).zfill(2))
                self.visibilities_filepath = np.str(self.sky_model_dir) + 'vis_' + str(sky_model_time) + '.npy'

                # Sky model to use
                self.sky_filepath = np.str(self.sky_files[i])

                self.generate_visibilities(distutils.util.strtobool(np.str(
                                          self.config.get('Settings', 'use_average_pattern'))),
                                          distutils.util.strtobool(np.str(
                                          self.config.get('Settings', 'use_flat_pattern'))))

        else:
            print 'Sky model generation not required. Reading sky models from source provided.'
            counter = 0
            for filename in sorted(glob.glob(self.sky_model_dir + 'sky*.npy')):
                print filename
                self.visibilities_filepath = np.str(self.sky_model_dir) + 'vis_' + str(counter).zfill(3) + '.npy'
                self.sky_filepath = filename
                self.generate_visibilities(distutils.util.strtobool(np.str(
                                          self.config.get('Settings', 'use_average_pattern'))),
                                          distutils.util.strtobool(np.str(
                                          self.config.get('Settings', 'use_flat_pattern'))))
                counter += 1

    def generate_errors(self):

        if distutils.util.strtobool(np.str(self.config.get('Error_Addition', 'real_visibilities_sim'))):

            counter = 0
            while True:
                phase_err = np.radians(np.float(self.config.get('Error_Addition', 'max_phase_offset_angle')))
                max_error = np.exp(1j * phase_err)
                sample_errors = np.random.uniform(low=-max_error.imag / 2, high=max_error.imag / 2, size=(256,))

                self.errors = np.random.normal(max_error.real + ((1. - max_error.real) / 2),
                                               ((1. - max_error.real) / 2), nof_elements) + \
                              np.random.normal(0., np.std(sample_errors), nof_elements) * 1j
                # noise_vis = np.random.normal(-0.1, 0.1, nof_baselines) + np.random.normal(-0.1, 0.1, nof_baselines)*1j
                # noise_vis = np.zeros(nof_baselines, dtype=np.complex)
                error_simulated = np.degrees(np.angle(self.errors))
                if (np.max(error_simulated) <= np.degrees(phase_err)) and (
                        np.min(error_simulated) >= -np.degrees(phase_err)):
                    # print 'Maximum +ve Phase Error: ' + str(np.max(error_simulated))
                    # print 'Maximum -ve Phase Error: ' + str(np.min(error_simulated))
                    break
                counter += 1
                if counter >= 100:
                    print 'Phase error too high. Simulation not constrained to given phase error.'
                    break

            np.save(self.config.get('Error_Addition', 'errors_filepath'), self.errors)

        else:
            self.errors = np.ones(nof_elements, dtype=np.complex128)
            error_simulated = np.degrees(np.angle(self.errors))
            # print 'Maximum +ve Phase Error: ' + str(np.max(error_simulated))
            # print 'Maximum -ve Phase Error: ' + str(np.min(error_simulated))
            np.save(self.config.get('Error_Addition', 'errors_filepath'), self.errors)

    def create_sky_model(self):
        time_dur = datetime.strptime(self.end_time, "%Y-%m-%dT%H:%M:%S") - \
                   datetime.strptime(self.start_time, "%Y-%m-%dT%H:%M:%S")
        time_dur_hours = (time_dur.days*24.) + (time_dur.seconds/3600.)
        self.num_time_steps = np.int(time_dur_hours / np.float(self.time_interval_hr))

        # Creating sky models
        for i in range(self.num_time_steps+1):
            current_time_step = datetime.strptime(self.start_time, "%Y-%m-%dT%H:%M:%S") + \
                                timedelta(hours=np.float(self.time_interval_hr)*i)
            self.current_time_step.append(current_time_step)
            sky_model_time = '{}_{}_{}_{}_{}_{}'.format(str(current_time_step.year).zfill(2),
                                                        str(current_time_step.month).zfill(2),
                                                        str(current_time_step.day).zfill(2),
                                                        str(current_time_step.hour).zfill(2),
                                                        str(current_time_step.minute).zfill(2),
                                                        str(current_time_step.second).zfill(2))
            sky_file_name = str(self.sky_model_dir) + 'sky_model_' + str(sky_model_time) + '.npy'
            SkyModelGenRun(current_time_step, filepath=sky_file_name)
            self.sky_files.append(sky_file_name)
            print 'Generating sky model for time step ' + str(i) + ' of ' + str(self.num_time_steps)

    def generate_spherical_beam(self):

        diff_deg = 180.0 / 512.
        phis = np.radians(np.arange(0., 360., diff_deg))
        phis = np.roll(phis, n_azimuth / 4, 0)
        thetas = np.radians(np.arange(90., 0. - diff_deg, -diff_deg))

        self.beam_x = np.outer(np.cos(thetas), np.cos(phis)).T
        self.beam_y = np.outer(np.cos(thetas), np.sin(phis)).T
        self.beam_z = np.outer(np.ones(len(phis)), np.sin(thetas))

        # Get beam plot
        # d_log = dot_theta[:, :, 20]

        # output_plot = [x, y, z, d_log]

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.set_aspect('auto')
        # ax.plot_surface(output_plot[0], output_plot[1], output_plot[2], rstride=1, cstride=1,
        #                facecolors=cm.RdYlBu(np.abs(output_plot[3])*10), alpha=0.3,
        #                linewidth=1, shade=True)
        # plt.show()
        # plt.close()

    def integrate_pairwise(self, index):
        # Each thread is responsible for n_threads combinations (for speed)

        for k in range(self.n_threads):
            # Get element indices
            ant_i, ant_j = self.indices[index * self.n_threads + k]

            # Compute dot product for theta
            dot_theta = (elements_theta[:, :, ant_i] * elements_theta[:, :, ant_j].conj()) * self.sin_theta
            dot_phi = (elements_phi[:, :, ant_i] * elements_phi[:, :, ant_j].conj()) * self.sin_theta

            # Compute visibility
            self.visibilities[ant_i, ant_j] = np.sum(sky[3, :, :] * (dot_theta + dot_phi))
            # (self.errors[ant_i] * np.conj(self.errors[ant_j]))
            # self.visibilities[ant_i, ant_j] = np.sum(dot_theta + dot_phi)
            self.global_counter += 1

        #sys.stdout.write("Finished processing {0} {1:.2f}%     \n".format
        #                 (self.global_counter, (self.global_counter / float(nof_baselines)) * 100))
        #sys.stdout.flush()

    def integrate_element_sky(self):
        """ Perform integration over entire sky and all patterns.
            n_threads defines whether this is performed in parallel"""

        # Integrate over all baselines
        if self.n_threads != 1:
            thread_pool = pool.ThreadPool(self.n_threads)
            thread_pool.map(self.integrate_pairwise, range(0, nof_baselines / self.n_threads))
        else:
            for index in range(nof_baselines):
                self.integrate_pairwise(index)

    def generate_average_element_pattern(self, use_flattened_pattern=False):
        """ Generate the average element pattern and set each element to the average """
        global coordinates
        global kx
        global ky

        k0 = 2 * np.pi / (c / self.f)
        n_thetas = n_elevation
        n_phis = n_azimuth

        # Load antenna locations
        coordinates = antenna_coordinates()

        # Compute kx and ky
        theta_range = np.radians(np.linspace(0, 90, n_thetas))
        phi_range = np.radians(np.linspace(0, 360, n_phis))
        kx = np.zeros((n_phis, n_thetas))
        ky = np.zeros((n_phis, n_thetas))
        for i in range(n_phis):
            for j in range(n_thetas):
                kx[i, j] = k0 * np.cos(phi_range[i]) * np.sin(theta_range[j])
                ky[i, j] = k0 * np.sin(phi_range[i]) * np.sin(theta_range[j])

        # If using flattened element pattern then we just assign the patterns to the location shift matrix
        if use_flattened_pattern:
            for i in range(nof_elements):
                # Compute total phase offset
                phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
                location_shift = np.exp(1j * phase_offset)

                # Phase shift the element pattern
                elements_theta[:, :, i] = location_shift
                elements_phi[:, :, i] = location_shift

            return  # All done

        # Loop over all the elements patters to compute average element pattern
        for i in range(nof_elements):
            # Compute total phase offset
            phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
            center_shift = np.exp(-1j * phase_offset)

            # Phase shift the element pattern
            elements_theta[:, :, i] = elements_theta[:, :, i] * center_shift
            elements_phi[:, :, i] = elements_phi[:, :, i] * center_shift

        # Compute average element pattern
        average_pattern_theta = np.mean(elements_theta, axis=2)
        average_pattern_phi = np.mean(elements_phi, axis=2)

        # Loop over all the elements patters to place phased average element pattern
        for i in range(nof_elements):
            # Compute total phase offset
            phase_offset = kx * coordinates[i, 0] + ky * coordinates[i, 1]
            location_shift = np.exp(1j * phase_offset)

            # Phase shift the element pattern
            elements_theta[:, :, i] = average_pattern_theta * location_shift
            elements_phi[:, :, i] = average_pattern_phi * location_shift

    def generate_visibilities(self, use_average_pattern, use_flat_pattern):
        global visibilities
        global elements_theta
        global elements_phi
        global sky

        sky = np.load(self.sky_filepath)

        # Check if visibilities have already been processed, if so load the visibility file
        if os.path.exists(self.visibilities_filepath):
            visibilities = np.load(self.visibilities_filepath)
        else:
            # Load the pre-processed sky map and 'rotate' by 90 degrees
            sky = np.load(self.sky_filepath)
            sky[0, :, :] = np.roll(sky[0, :, :], n_azimuth / 4, axis=0)
            sky[1, :, :] = np.roll(sky[1, :, :], n_azimuth / 4, axis=0)
            sky[2, :, :] = np.roll(sky[2, :, :], n_azimuth / 4, axis=0)
            sky[3, :, :] = np.roll(sky[3, :, :], n_azimuth / 4, axis=0)

            # Load mat file containing element patterns
            with h5py.File(self.element_filepath, 'r') as element_file:
                # Load theta and phi and convert to numpy complex
                elements_theta = element_file['dipole_pattern_theta'][:].T
                elements_theta = elements_theta['real'] + elements_theta['imag'] * 1j
                elements_phi = element_file['dipole_pattern_phi'][:].T
                elements_phi = elements_phi['real'] + elements_phi['imag'] * 1j

            # Select required range from element patterns
            elements_theta = elements_theta[:n_azimuth, :n_elevation, :]
            elements_phi = elements_phi[:n_azimuth, :n_elevation, :]

            self.generate_spherical_beam()

            # Test for alignment and compute visibilities
            iii = 0
            if np.where(sky[0, :, :] == self.beam_x[iii, iii])[0][0] == iii:
                print 'Sky and element pattern projections aligned successfully.'

            else:
                print 'Error: Misaligned beam and sky projections detected!'
                exit()

            # If average pattern is required, compute element theta and phi averages
            if use_average_pattern:
                print 'Averaging element patterns.'
                self.generate_average_element_pattern()
            if use_flat_pattern:
                print 'Flattening element patterns.'
                self.generate_average_element_pattern(use_flattened_pattern=True)
            if not use_average_pattern:
                if not use_flat_pattern:
                    print 'Using embedded element patterns.'

            # Perform integration
            t0 = time.time()
            self.integrate_element_sky()
            t1 = time.time()
            print "Generated visibilities in {0:.2f}s".format(t1 - t0)

            # Save visibilities
            np.save(self.visibilities_filepath, self.visibilities)

