import scipy.io as sio
import numpy as np
import datetime
from math import *
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
from scipy import spatial
import logging


class SkyModelGen:

    def __init__(self, utc_time, data, filepath):

        logging.basicConfig()
        logging.getLogger().setLevel(logging.INFO)

        self.time = utc_time
        self.data = data
        self.filepath = filepath
        self.sky_file = None
        self._logger = logging.getLogger(__name__)

        self._gen_sky_model()

    def _juliandate(self):
        """Returns the Julian day number of a date."""

        year_duration = 365.25

        year = self.time.year
        month = self.time.month
        day = self.time.day
        hour = self.time.hour
        minute = self.time.minute
        sec = self.time.second

        if month <= 2:
            year = year - 1
            month = month + 12

        a = floor(year_duration * (year + 4716))
        b = floor(30.6001 * (month + 1))
        c = 2
        d = floor(year / 100)
        e_ = floor(floor(year / 100) * .25)
        f = day - 1524.5
        g = (hour + (minute / 60.0) + sec / 3600.0) / 24.0
        return a + b + c - d + e_ + f + g

    def _azel2radec(self, azim, elev, lat, lon):
        """Returns Ra, Dec of specific Az, El point for a given location and time"""

        jd = self._juliandate()
        t_ut1 = (jd - 2451545) / 36525
        theta_gmst = 67310.54841 + (876600 * 3600 + 8640184.812866) \
            * t_ut1 + 0.093104 * (t_ut1 ** 2) - 6.2 * 10e-6 * (t_ut1 ** 3)
        theta_gmst = fmod((fmod(theta_gmst, 86400 * (theta_gmst / abs(theta_gmst))) / 240), 360)

        theta_lst = theta_gmst + lon

        lat = radians(lat)
        azim = radians(azim)
        elev = radians(elev)

        dec = asin(sin(elev) * sin(lat) + cos(elev) * cos(lat) * cos(azim))
        lha = atan2(-sin(azim) * cos(elev) / cos(dec),
                    (sin(elev) - sin(dec) * sin(lat)) / (cos(dec) * cos(lat))) * (180 / pi)
        ra = (theta_lst - lha) % 360

        return ra, degrees(dec)

    @staticmethod
    def _find_nearest(array, value):
        """Return nearest value to a specified value in an array"""

        idx = (np.abs(array - value)).argmin()
        return idx

    def _compute_spherical_map(self, el, az, el_az_sky):

        data_labels_dec_rad = np.radians(el)
        data_labels_ra_rad = np.radians(az)

        # Determine x,y,z spherical projection of RA, Dec values
        theta, phi = data_labels_dec_rad, data_labels_ra_rad
        x = np.outer(np.cos(theta), np.cos(phi)).T
        y = np.outer(np.cos(theta), np.sin(phi)).T
        z = np.outer(np.ones(len(phi)), np.sin(theta))

        d = el_az_sky.T

        # d_log = np.log(np.log(np.log(d)))

        output = np.array([x, y, z, d])
        # output_plot = [x, y, z, d_log]

        # Save spherical sky model to file
        np.save(self.filepath, output)

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.set_aspect('auto')
        # ax.plot_surface(output_plot[0], output_plot[1], output_plot[2], rstride=1, cstride=1,
        # facecolors=cm.RdYlBu(output_plot[3]), alpha=0.3,
        #                 linewidth=1, shade=True)
        # plt.savefig('C:/Users/Josef Borg/Documents/PythonProjects/AAVS_Calibration/spherical_projections/2/' +
        #            str(int(self.time.hour)) + '.png', dpi=300)
        # plt.close()

        # return x, y, z, d

    def _gen_sky_model(self):

        # run_start = datetime.datetime.now()

        # Telescope location longitude, latitude
        latit = -26.70408
        longit = 116.670231

        # Selecting declination, RA at degree intervals
        diff_deg = 180.0 / self.data.shape[0]
        data_labels_dec = np.arange(-90., 90., diff_deg)
        data_labels_ra = np.arange(360., 0., -diff_deg)

        # Get RA Dec and closest corresponding sky power for defined El Az coordinates
        el = np.arange(0., 90. + diff_deg, diff_deg)
        az = np.arange(0., 360., diff_deg)

        # Factor in frequency scaling, if necessary
        ###

        # Map el_az sky from matching ra_dec coordinates
        num_pixels = len(el) * len(az)
        el_az_sky = np.zeros((len(el), len(az)))
        declinations = np.zeros(num_pixels)
        right_ascensions = np.zeros(num_pixels)
        counter = 0
        for i in range(len(el)):
            for j in range(len(az)):
                ra_val, dec_val = self._azel2radec(elev=el[i], azim=az[j], lat=latit, lon=longit)
                ra_match = self._find_nearest(data_labels_ra, ra_val)
                dec_match = self._find_nearest(data_labels_dec, dec_val)

                declinations[counter] = data_labels_dec[dec_match]
                right_ascensions[counter] = data_labels_ra[ra_match]

                el_az_sky[i, j] = self.data[dec_match, ra_match]

                counter += 1

        # tree = spatial.cKDTree(np.c_[declinations.ravel(), right_ascensions.ravel()])
        # counter = 0
        # map_start = datetime.datetime.now()
        # for x in range(len(el)):
        #     for y in range(len(az)):
        #         el_az_sky[x, y] /= len(tree.query_ball_point([declinations[counter], right_ascensions[counter]],
        # r=0.))
        #         counter += 1

        self._compute_spherical_map(el, az, el_az_sky)

        # time_end = datetime.datetime.now()
        # self._logger.info('Finished RA,Dec to Alt,Az map in: ' + str((time_end - map_start).seconds +
        #                                                             (time_end - map_start).microseconds*1e-6) + 's')
        # self._logger.info('Finished model sky simulation in: ' + str((time_end - run_start).seconds +
        #                                                             (time_end - run_start).microseconds*1e-6) + 's')

        # Save sky model to file
        # name = '{}_{}_{}_{}_{}_{}.png'.format(str(self.time.year), str(self.time.month), str(self.time.day),
        #                                      str(self.time.hour), str(self.time.minute), str(self.time.second))
        # np.save('C:/Users/Josef Borg/Documents/PythonProjects/AAVS_Calibration/sky_model/'
        #        'sky_model_test/sky_model_ElAz_' + name + '.npy', el_az_sky.T)

        # Plot log representation of saved El Az sky model if required
        # plt.figure()
        # plt.imshow(np.log(el_az_sky), cmap='RdYlBu', aspect='auto')
        # plt.colorbar()
        # plt.title('UTC Time: ' + str(self.time))
        # plt.savefig('C:/Users/Josef Borg/Desktop/SMs2/sky_model_ElAz_' + name + '.png', dpi=300)


class SkyModelGenRun:

    def __init__(self, datetime_start, filepath):

        # Load full sky model
        mat_loaded = sio.loadmat('C:/Users/Josef Borg/Documents/PythonProjects/AAVS_Calibration/'
                                 'aavs1_simulator_resources/sky408_radec.mat')

        data = mat_loaded['Y']

        time_start = datetime.datetime(year=datetime_start.year, month=datetime_start.month, day=datetime_start.day,
                                       hour=datetime_start.hour, minute=datetime_start.minute,
                                       second=datetime_start.second)

        SkyModelGen(time_start, data, filepath)
